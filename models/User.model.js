const mongoose = require('mongoose');

const Genders = ["Male", "Female"];

var userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    dob: {
        type: Date,
        required: true
    },
    gender: {
         type: String,
         enum: Genders,
         required : true
    },
    email: {
        type: String,
        
        required : true
    },
    password:{
        type: String,
        required: true
    },
    mobile: {
        type: String,
         required: true
    }
});

// // Custom validation for email
// userSchema.path('email').validate((val) => {
//     emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     return emailRegex.test(val);
// }, 'Invalid e-mail.');

mongoose.model('userModel', userSchema);