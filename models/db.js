const mongoose = require('mongoose');
require('dotenv/config');

mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true , useUnifiedTopology: true }, (err) => {
    if (!err) { console.log('MongoDB Connection Successfull.') }
    else { console.log('Error in DB connection : ' + err) }
});

require('./User.model');
require('./Upload.model');
require('./User.Role.model');