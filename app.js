require('./models/db');
const User = require('./models/User.Role.model');
const express = require('express');
const bodyparser = require('body-parser');
// const jwt = require('jsonwebtoken');
const routes = require('./routes/route.js');
const jwt = require('jsonwebtoken');

const userController = require('./controllers/user.controller');
const Rolecontroll = require('./controllers/role.controller');
const userProfile = require('./profile');
const uploader = require('./uploaders');

var app = express();
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
// app.use('/Users',userController);
app.use('/',userController);
app.use('/',userProfile);
app.use('/',uploader);
app.use('/',routes);


app.use(async (req, res, next) => {
 if (req.headers["x-access-token"]) {
  const accessToken = req.headers["x-access-token"];
  const { userId, exp } = await jwt.verify(accessToken, process.env.TOKEN_SECRET);
  // Check if token has expired
  if (exp < Date.now().valueOf() / 1000) { 
   return res.status(401).json({ error: "JWT token has expired, please login to obtain a new one" });
  } 
  console.log(" app function 1"+res.locals.loggedInUser);
  res.locals.loggedInUser = await User.findOne(userId); 
  console.log(" app function 2"+res.locals.loggedInUser);
  next();
   
 } else { 
  next(); 
 } 
});

app.listen(3000, () => {
    console.log('Express server started at port : 3000');
});


