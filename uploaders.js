var express = require('express')
var router = express.Router();
var multer = require('multer');
const mongoose = require('mongoose');
const UploadFiles = mongoose.model('uploadFiles');
const fs = require('fs');

// const upload = multer({
//   dest: './uploads'
// }).single('image'); 

// console.log(upload);

// router.post('/uploaders', upload, (req, res) => {
//    var Imagename = req.file.originalname;
//   res.send('uploaded' +Imagename);
// });

// module.exports = router;

 
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
var upload = multer({ storage: storage }); 

router.post('/uploadfile', upload.single('myFile'), async (req, res, next) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    return next(error)
  }
  
    // insertimg(req,res);
    var photo = './uploads/' +file.originalname;

    var Uploadimage = new UploadFiles();
    //  Uploadimage.img = image;

    Uploadimage.img.data = fs.readFileSync(photo)
    Uploadimage.img.contentType = 'image/png , image/jpg';

      Uploadimage.save((err,doc) => {

      if(!err){
          res.json(doc);
          console.log('imaged saved in db successfully');
          
      }else{
          console.log(err.message);
      }
  });
})


router.get('/displayImage/:id',async(req,res) => {

  const photos = await UploadFiles.findOne({_id:req.params.id});
  var images = new Buffer(photos.img.data).toString('base64');
  res.json(images);
})



// function insertimg(req,res){

// var Uploadimage = new UploadFiles();
//      Uploadimage.img = image;
//       Uploadimage.save((err,doc) => {

//       if(!err){
//           res.json(doc);
//           console.log('imaged saved in db successfully');
          
//       }else{
//           console.log(err.message);
//       }
//   });

// }


// var image = './uploads/' +file.originalname;
//     res.send(image);
//      var Uploadimage = new UploadFiles();
//      Uploadimage.img = image;
//      await Uploadimage.save((err,doc) => {

//       if(!err){
//           res.json(doc);
//           console.log('User Created Successfully');
          
//       }else{
//           console.log(err.message);
//       }
//   });



// router.post('/uploadmultiple', upload.array('myFiles', 12), (req, res, next) => {
//   const files = req.files
//   if (!files) {
//     const error = new Error('Please choose files')
//     return next(error)
//   }
//     res.send(files)
// })


module.exports = router;

