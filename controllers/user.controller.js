const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const UserModel = mongoose.model('userModel');
const nodemailer = require('nodemailer');
const sendmail = require('sendmail');
const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');


const {registrationValidation} = require('../validations');



router.post('/Signup', async (req,res) => {

    const {error} = registrationValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const emailExist = await UserModel.findOne({email: req.body.email});
    if(emailExist) return res.status(400).send('Email Already Exists!!!');

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password,salt);

        var userModel = new UserModel();

        userModel.name = req.body.name;
        userModel.dob = req.body.dob;
        userModel.gender = req.body.gender;
        userModel.email = req.body.email;
        userModel.password = hashedPassword;
        userModel.mobile = req.body.mobile;

       await userModel.save((err,doc) => {

            if(!err){
                res.json(doc);
              
                console.log('User Created Successfully');

                var transporter = nodemailer.createTransport({
                    
                    service: 'gmail',
                    auth: {
                      user: 'himanshusonkar84@gmail.com',
                      pass: 'your password here'
                    }
                  });
                  
                  var mailOptions = {
                    from: 'himanshusonkar84@gmail.com',
                    to: req.body.email,
                    subject: 'Registration Successfull ' + req.body.name,
                    text: "'Welcome to Profile!' "+ req.body.name
                  };
                  
                  transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                      console.log(error);
                    } else {
                      // console.log('Email sent: ' + info.response);
                      console.log('Email sent successfully ');
                    }
                  }); 
                
            }else{
                console.log(err.message);
            }
        });
    })


    router.post('/login', async (req,res) => {
        console.log(req.body.email);
        const user = await UserModel.findOne({email : req.body.email});
        if(!user) return res.status(400).send('Email not found');

        const pass = await bcrypt.compare(req.body.password,user.password);
        if(!pass) return res.status(400).send('Invalid password');

        // res.send('Logged IN!!!');

        const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET);
        res.header('auth-token',token).send(token);

    })



    router.get('/allUsers', async (req,res) => {

        const Users = await UserModel.find();
        res.json(Users);
     
    })    

    router.get('/findUser/:id',async(req,res) => {

        const Users = await UserModel.findOne({_id:req.params.id});
        res.json(Users);
    })

    router.put('/updateUser/:id',async(req,res) => {
        try{
          const updated = await UserModel.updateOne(
            {_id: req.params.id }, { $set : req.body }, {new : true}
          );
          res.send("updated  successfully");
        } catch (err) {
          res.json({ message : err});
        }
        
        // const Users = await UserModel.findOneAndUpdate( {_id: req.params.id },req.body,{new:true});
        // res.json(Users);
     
    })    

    router.delete('/deleteUser/:id',async(req,res) => {

        const Users = await UserModel.findOneAndDelete({_id:req.params.id});
        res.json(Users);
        // res.send('User Deleted');
     
    })

    module.exports = router;

