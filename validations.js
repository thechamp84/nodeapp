const Joi = require('@hapi/joi');

const registrationValidation = data => {

const schema = Joi.object({

name: Joi.string().min(3).required(),
dob: Joi.date().required(),
gender: Joi.string().required(),
email: Joi.string().email().required(),
password: Joi.string().min(5).required(),
mobile: Joi.string().min(10).required() 

});
 return schema.validate(data);
 
};

module.exports.registrationValidation = registrationValidation;