
const express = require('express');
const router = express.Router();
const userController = require('../controllers/role.Controller');
 
router.post('/register', userController.signup);
 
router.post('/Rolelogin', userController.login);
 
router.get('/userRole/:userId', userController.allowIfLoggedin, userController.getUser);
 
router.get('/usersRoles', userController.allowIfLoggedin, userController.grantAccess('readAny', 'profile'), userController.getUsers);
 
router.put('/userRole/:userId', userController.allowIfLoggedin, userController.grantAccess('updateAny', 'profile'), userController.updateUser);
 
router.delete('/userRole/:userId', userController.allowIfLoggedin, userController.grantAccess('deleteAny', 'profile'), userController.deleteUser);
 
module.exports = router;